<?php
/**
 * Реализовать проверку заполнения обязательных полей формы в предыдущей
 * с использованием Cookies, а также заполнение формы по умолчанию ранее
 * введенными значениями.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    // Выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
    // Если в куках есть пароль, то выводим сообщение.
    if (!empty($_COOKIE['pass'])) {
      $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }
  }


  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['name'] = !empty($_COOKIE['name_error']);
  $errors['e-mail'] = !empty($_COOKIE['e-mail_error']);
  $errors['date'] = !empty($_COOKIE['date_error']);
  $errors['Tarelki'] = !empty($_COOKIE['Tarelki_error']);
  $errors['letter'] = !empty($_COOKIE['letter_error']);
  $errors['radio1'] = !empty($_COOKIE['radio1_error']);
  $errors['radio0'] = !empty($_COOKIE['radio0_error']);
  $errors['ocenka'] = !empty($_COOKIE['ocenka_error']);

  // TODO: аналогично все поля.

  // Выдаем сообщения об ошибках.
  if ($errors['name']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('name_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните корректно имя.</div>';
  }
  if ($errors['e-mail']) {
    setcookie('e-mail_error', '', 100000);
    $messages[] = '<div class="error">Заполните корректно email.</div>';
  }
  if ($errors['date']) {
    setcookie('date_error', '', 100000);
    $messages[] = '<div class="error">Вы забыли заполнить дату.</div>';
  }
  if ($errors['Tarelki']) {
    setcookie('Tarelki_error', '', 100000);
    $messages[] = '<div class="error">Вы забыли выбрать формы тарелок.</div>';
  }
  if ($errors['letter']) {
    setcookie('letter_error', '', 100000);
    $messages[] = '<div class="error">Вы забыли рассказать о Ваших тарелках.</div>';
  }
  if ($errors['radio1']) {
    setcookie('radio1_error', '', 100000);
    $messages[] = '<div class="error">Вы забыли заполнить отметить тарелки, которые Вам нравятся.</div>';
  }
  if ($errors['radio0']) {
    setcookie('radio0_error', '', 100000);
    $messages[] = '<div class="error">Вы забыли отметить, что Вы кушаете на завтрак.</div>';
  }
  if ($errors['ocenka']) {
    setcookie('ocenka_error', '', 100000);
    $messages[] = '<div class="error">Вы забыли оценить мою работу.</div>';
  }
  


  // TODO: тут выдать сообщения об ошибках в других полях.

  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();
  
  // TODO: аналогично все поля.
  $values['name'] = empty($_COOKIE['name_value']) ? '' : strip_tags($_COOKIE['name_value']);
  $values['e-mail'] = empty($_COOKIE['e-mail_value']) ? '' : strip_tags($_COOKIE['e-mail_value']);
  $values['date'] = empty($_COOKIE['date_value']) ? '' : strip_tags($_COOKIE['date_value']);
  $values['Tarelki'] = empty($_COOKIE['Tarelki_value']) ? '' : strip_tags($_COOKIE['Tarelki_value']);
  $values['letter'] = empty($_COOKIE['letter_value']) ? '' : strip_tags($_COOKIE['letter_value']);
  $values['radio1'] = empty($_COOKIE['radio1_value']) ? '' : strip_tags($_COOKIE['radio1_value']);
  $values['radio0'] = empty($_COOKIE['radio0_value']) ? '' : strip_tags($_COOKIE['radio0_value']);
  $values['ocenka'] = empty($_COOKIE['ocenka_value']) ? '' : strip_tags($_COOKIE['ocenka_value']);
  $err=false;
  foreach($errors as $ell)
  if ($ell==true)
  $err=true;
  if ($err==false && !empty($_COOKIE[session_name()]) &&
  session_start() && !empty($_SESSION['login'])) {
$user = 'u24119';
$pass = '3482660';
$db = new PDO('mysql:host=localhost;dbname=u24119', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
$uid=$_SESSION['uid'];
$res=$db->query("SELECT name,email,DataBr,letter_about_plates,breakfast,love_of_plates, ocenka FROM application_2 WHERE id=$uid");
foreach($res as $ell){
  $values['name']=strip_tags($ell['name']);
  $values['e-mail']=strip_tags($ell['email']);
  $values['date']=strip_tags($ell['DataBr']);
  $values['letter']=strip_tags($ell['letter_about_plates']);
  $values['radio1']=strip_tags($ell['breakfast']);
  $values['radio0']=strip_tags($ell['love_of_plates']);
  $values['ocenka']=strip_tags($ell['ocenka']);

}
$res=$db->query("SELECT nomer FROM plate_shape WHERE user_id=$uid");
$tar=array();
foreach($res as $ell){
  $tar[]=(int)strip_tags($ell['nomer']);
}
$t=implode('',$tar);
$values['Tarelki']=$t;
printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
}
  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
  if (empty($_POST['name']) || preg_match('/[^(\x7F-\xFF)|(\s)]/', $_POST['name'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('name_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['e-mail'])) {
    setcookie('e-mail_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('e-mail_value', $_POST['e-mail'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['date'])) {
    setcookie('date_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('date_value', $_POST['date'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['Tarelki'])) {
    setcookie('Tarelki_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    $t=implode('',$_POST['Tarelki']);
    setcookie('Tarelki_value', $t, time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['letter'])) {
    setcookie('letter_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('letter_value', $_POST['letter'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['radio1'])) {
    setcookie('radio1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('radio1_value', $_POST['radio1'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['radio0'])) {
    setcookie('radio0_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('radio0_value', $_POST['radio0'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['ocenka'])) {
    setcookie('ocenka_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('ocenka_value', $_POST['ocenka'], time() + 30 * 24 * 60 * 60);
  }

// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('name_error', '', 100000);
    // TODO: тут необходимо удалить остальные Cookies.
    setcookie('e-mail_error', '', 100000);
    setcookie('date_error', '', 100000);
    setcookie('Tarelki_error', '', 100000);
    setcookie('letter_error', '', 100000);
    setcookie('radio1_error', '', 100000);
    setcookie('radio0_error', '', 100000);
    setcookie('ocenka_error', '', 100000);
  }
  $user = 'u24119';
$pass = '3482660';
$db = new PDO('mysql:host=localhost;dbname=u24119', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
        $uid=$_SESSION['uid'];
        $stmt=$db->prepare("UPDATE application_2 SET name=?,email=?,DataBr=?,letter_about_plates=?,breakfast=?,love_of_plates=?, ocenka=? WHERE id=$uid");
        $stmt->execute([$_POST['name'],$_POST['e-mail'],$_POST['date'],$_POST['letter'],$_POST['radio1'],$_POST['radio0'],$_POST['ocenka']]);
        $db->query("DELETE FROM plate_shape WHERE user_id=$uid");
        $stmt=$db->prepare("INSERT INTO plate_shape SET user_id=?,nomer=?");
        foreach($_POST['Tarelki'] as $ell){
          $stmt->execute([$uid,$ell]);
        }
    // TODO: перезаписать данные в БД новыми данными,
    // кроме логина и пароля.
  }
  else {
    // Генерируем уникальный логин и пароль.
    // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
    $login = substr(uniqid(time()), 1,8);
    $pass = substr( md5($_POST['e-mail']),5,8);
    // Сохраняем в Cookies.
    setcookie('login', $login);
    setcookie('pass', $pass);
    
$Tarelki= implode(',',$_POST['Tarelki']);
// Подготовленный запрос. Не именованные метки.
try {
  $stmt = $db->prepare("INSERT INTO application_2 SET name = ?, email = ?, DataBr = ?, letter_about_plates = ?, breakfast = ?, love_of_plates = ?, ocenka = ?");
  $stmt -> execute([$_POST['name'],$_POST['e-mail'],$_POST['date'],$_POST['letter'],$_POST['radio1'],$_POST['radio0'],$_POST['ocenka']]);
  $stmt = $db->prepare("INSERT INTO plate_shape SET user_id = ?, nomer = ?");
  $id = $db->lastInsertId();
  foreach ($_POST['Tarelki'] as $per) { 
    $stmt-> execute([$id,$per]);
  } 
  $stmt=$db->prepare("INSERT INTO loginparol SET id=?,login=?,parol=?");
  $stmt->execute([$id,$login,md5($pass)]);
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}

    // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
    // ...
  }
 

  // Сохранение в XML-документ.
  // ...

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');
}
